import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'start',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'start',
    loadChildren: () => import('./pages/start/start.module').then( m => m.StartPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'emergency',
    loadChildren: () => import('./pages/emergency/emergency.module').then( m => m.EmergencyPageModule)
  },
  {
    path: 'directory',
    loadChildren: () => import('./pages/directory/directory.module').then( m => m.DirectoryPageModule)
  },
  {
    path: 'cctv',
    loadChildren: () => import('./pages/cctv/cctv.module').then( m => m.CctvPageModule)
  },
  {
    path: 'communication',
    loadChildren: () => import('./pages/communication/communication.module').then( m => m.CommunicationPageModule)
  },
  {
    path: 'order',
    loadChildren: () => import('./pages/order/order.module').then( m => m.OrderPageModule)
  },
  {
    path: 'advertisement',
    loadChildren: () => import('./pages/advertisement/advertisement.module').then( m => m.AdvertisementPageModule)
  },
  {
    path: 'announcement',
    loadChildren: () => import('./pages/announcement/announcement.module').then( m => m.AnnouncementPageModule)
  },
  {
    path: 'service-desk',
    loadChildren: () => import('./pages/service-desk/service-desk.module').then( m => m.ServiceDeskPageModule)
  },
  {
    path: 'facility',
    loadChildren: () => import('./pages/facility/facility.module').then( m => m.FacilityPageModule)
  },
  {
    path: 'billing',
    loadChildren: () => import('./pages/billing/billing.module').then( m => m.BillingPageModule)
  },
  {
    path: 'administration',
    loadChildren: () => import('./pages/administration/administration.module').then( m => m.AdministrationPageModule)
  },
  {
    path: 'tenant-registration',
    loadChildren: () => import('./pages/administration/menu/tenant-registration/tenant-registration.module').then( m => m.TenantRegistrationPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./pages/administration/menu/change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'dispute',
    loadChildren: () => import('./pages/billing/menu/dispute/dispute.module').then( m => m.DisputePageModule)
  },
  {
    path: 'process-dispute',
    loadChildren: () => import('./components/process-dispute/process-dispute.module').then( m => m.ProcessDisputePageModule)
  },
  {
    path: 'invoice-view',
    loadChildren: () => import('./components/invoice-view/invoice-view.module').then( m => m.InvoiceViewPageModule)
  },
  {
    path: 'invoice',
    loadChildren: () => import('./pages/billing/menu/invoice/invoice.module').then( m => m.InvoicePageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./pages/billing/menu/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'process-payment',
    loadChildren: () => import('./components/process-payment/process-payment.module').then( m => m.ProcessPaymentPageModule)
  },
  {
    path: 'booking',
    loadChildren: () => import('./pages/facility/menu/booking/booking.module').then( m => m.BookingPageModule)
  },
  {
    path: 'facility-order',
    loadChildren: () => import('./pages/facility/menu/facility-order/facility-order.module').then( m => m.FacilityOrderPageModule)
  },
  {
    path: 'facility-payment',
    loadChildren: () => import('./pages/facility/menu/facility-payment/facility-payment.module').then( m => m.FacilityPaymentPageModule)
  },
  {
    path: 'facility-schedule',
    loadChildren: () => import('./pages/facility/menu/facility-schedule/facility-schedule.module').then( m => m.FacilitySchedulePageModule)
  },
  {
    path: 'complaint',
    loadChildren: () => import('./pages/service-desk/menu/complaint/complaint.module').then( m => m.ComplaintPageModule)
  },
  {
    path: 'incident',
    loadChildren: () => import('./pages/service-desk/menu/incident/incident.module').then( m => m.IncidentPageModule)
  },
  {
    path: 'request',
    loadChildren: () => import('./pages/service-desk/menu/request/request.module').then( m => m.RequestPageModule)
  },
  {
    path: 'suggestion',
    loadChildren: () => import('./pages/service-desk/menu/suggestion/suggestion.module').then( m => m.SuggestionPageModule)
  },
  {
    path: 'work-order',
    loadChildren: () => import('./pages/service-desk/menu/work-order/work-order.module').then( m => m.WorkOrderPageModule)
  },
  {
    path: 'report',
    loadChildren: () => import('./pages/service-desk/menu/report/report.module').then( m => m.ReportPageModule)
  },
  {
    path: 'general',
    loadChildren: () => import('./pages/announcement/menu/general/general.module').then( m => m.GeneralPageModule)
  },
  {
    path: 'government',
    loadChildren: () => import('./pages/announcement/menu/government/government.module').then( m => m.GovernmentPageModule)
  },
  {
    path: 'holidays',
    loadChildren: () => import('./pages/announcement/menu/holidays/holidays.module').then( m => m.HolidaysPageModule)
  },
  {
    path: 'maintenance',
    loadChildren: () => import('./pages/announcement/menu/maintenance/maintenance.module').then( m => m.MaintenancePageModule)
  },
  {
    path: 'public',
    loadChildren: () => import('./pages/announcement/menu/public/public.module').then( m => m.PublicPageModule)
  },
  {
    path: 'internal',
    loadChildren: () => import('./pages/advertisement/menu/internal/internal.module').then( m => m.InternalPageModule)
  },
  {
    path: 'public-service',
    loadChildren: () => import('./pages/advertisement/menu/public-service/public-service.module').then( m => m.PublicServicePageModule)
  },
  {
    path: 'tenants',
    loadChildren: () => import('./pages/advertisement/menu/tenants/tenants.module').then( m => m.TenantsPageModule)
  },
  {
    path: 'food',
    loadChildren: () => import('./pages/order/menu/food/food.module').then( m => m.FoodPageModule)
  },
  {
    path: 'food-detail',
    loadChildren: () => import('./pages/order/menu/food/food-detail/food-detail.module').then( m => m.FoodDetailPageModule)
  },
  {
    path: 'goods',
    loadChildren: () => import('./pages/order/menu/goods/goods.module').then( m => m.GoodsPageModule)
  },
  {
    path: 'process-order-goods',
    loadChildren: () => import('./components/process-order-goods/process-order-goods.module').then( m => m.ProcessOrderGoodsPageModule)
  },
  {
    path: 'service',
    loadChildren: () => import('./pages/order/menu/service/service.module').then( m => m.ServicePageModule)
  },
  {
    path: 'process-order-service',
    loadChildren: () => import('./components/process-order-service/process-order-service.module').then( m => m.ProcessOrderServicePageModule)
  },
  {
    path: 'guest',
    loadChildren: () => import('./pages/communication/menu/guest/guest.module').then( m => m.GuestPageModule)
  },
  {
    path: 'parcel',
    loadChildren: () => import('./pages/communication/menu/parcel/parcel.module').then( m => m.ParcelPageModule)
  },
  {
    path: 'receptionist',
    loadChildren: () => import('./pages/communication/menu/receptionist/receptionist.module').then( m => m.ReceptionistPageModule)
  },
  {
    path: 'security',
    loadChildren: () => import('./pages/communication/menu/security/security.module').then( m => m.SecurityPageModule)
  },
  {
    path: 'building-management',
    loadChildren: () => import('./pages/directory/menu/building-management/building-management.module').then( m => m.BuildingManagementPageModule)
  },
  {
    path: 'business-tenants',
    loadChildren: () => import('./pages/directory/menu/business-tenants/business-tenants.module').then( m => m.BusinessTenantsPageModule)
  },
  {
    path: 'emergency-numbers',
    loadChildren: () => import('./pages/directory/menu/emergency-numbers/emergency-numbers.module').then( m => m.EmergencyNumbersPageModule)
  },
  {
    path: 'public-numbers',
    loadChildren: () => import('./pages/directory/menu/public-numbers/public-numbers.module').then( m => m.PublicNumbersPageModule)
  },
  {
    path: 'residents',
    loadChildren: () => import('./pages/directory/menu/residents/residents.module').then( m => m.ResidentsPageModule)
  },
  {
    path: 'emergency-button',
    loadChildren: () => import('./pages/emergency/menu/emergency-button/emergency-button.module').then( m => m.EmergencyButtonPageModule)
  },
  {
    path: 'emergency-report',
    loadChildren: () => import('./pages/emergency/menu/emergency-report/emergency-report.module').then( m => m.EmergencyReportPageModule)
  },
  {
    path: 'emergency-response',
    loadChildren: () => import('./pages/emergency/menu/emergency-response/emergency-response.module').then( m => m.EmergencyResponsePageModule)
  },
  {
    path: 'process-facility-payment',
    loadChildren: () => import('./components/process-facility-payment/process-facility-payment.module').then( m => m.ProcessFacilityPaymentPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
