import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController, Platform } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-emergency-button',
  templateUrl: './emergency-button.page.html',
  styleUrls: ['./emergency-button.page.scss'],
})
export class EmergencyButtonPage implements OnInit {

  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		//private geolocation: Geolocation,
		private platform: Platform,
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		//private callNumber: CallNumber
  ) { }

  ngOnInit() {


		console.log('ionViewDidLoad EmergencyButtonPage');
	}

	showMyLocation() {
		// this.platform.ready().then(() => {
		// 	let loader = this.loadingCtrl.create({
		// 		content: "Please wait..."
		// 	});
		// 	let param = { lat: 0, long: 0, tenant_id: '', site: '' };
		// 	loader.present();

		// 	this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((resp) => {
		// 		param.lat = resp.coords.latitude;
		// 		param.long = resp.coords.longitude;
		// 		this.storage.get('currentUser').then((storage: any) => {
		// 			param.tenant_id = storage.id;
		// 			param.site = storage.site;
		// 			this.api.postData('emergency/button/save_my_location', param).subscribe((data) => {
		// 				this.response = data;
		// 				loader.dismiss();

		// 				this.callNumber.callNumber("0883873375408", true)
		// 					.then(res => console.log('Launched dialer!', res))
		// 					.catch(err => console.log('Error launching dialer', err));

		// 			});
		// 		});
		// 	});
		// });
	}

  back() {
    this.navCtrl.back();
  }

  home(){
	  this.navCtrl.navigateRoot('home');
  }
}
