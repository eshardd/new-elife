import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController, Platform } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-emergency-report',
  templateUrl: './emergency-report.page.html',
  styleUrls: ['./emergency-report.page.scss'],
})
export class EmergencyReportPage implements OnInit {

  tenant_name:string;
  report: any = 'messages';
  emergencyreportList: any;
  response:any;
  Form : FormGroup;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) { 
    this.emergencyreportList = [
      {'id': 1, 'title': 'Kebakaran 16 sept 2018', 'description': 'Terjadi kebakaran pada lantai 17'},
    ];
    this.Form = this.formBuilder.group({
     tenant_id    :   [''],
     title    :   ['', Validators.required],
     messages    :   ['', Validators.required],
   });
  }

  ngOnInit(){
    this.storage.get('currentUser').then((storage: any)=> {
       this.tenant_name = storage.name;
     });
    //this.getdata();
  }

  getdata(){
    this.loading.present("Please wait...");
     this.storage.get('currentUser').then((storage: any)=> {
       this.api.getData('emergency/report/list/'+storage.id).subscribe((data)=> {
        this.response = data;
        this.loading.dismiss();
      });
     });
   }

  process(){
    this.loading.present("Please wait...");
    this.storage.get('currentUser').then((storage: any)=> {
      this.Form.value.tenant_id   = storage.id;
      this.Form.value.status      = 0;
      this.api.postData('emergency/report/add',this.Form.value).subscribe((data)=> {
        if (data != undefined) {
          this.toastr.present("Success","top");
          this.navCtrl.pop();
        }
      });
      this.loading.dismiss();
    });
   }

  back() {
    this.navCtrl.back();
  }

  home(){
    this.navCtrl.navigateRoot('home');
  }

}
