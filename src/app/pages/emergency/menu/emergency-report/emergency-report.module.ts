import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmergencyReportPageRoutingModule } from './emergency-report-routing.module';

import { EmergencyReportPage } from './emergency-report.page';
import { NavParams } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    EmergencyReportPageRoutingModule
  ],
  declarations: [EmergencyReportPage],
  providers:[NavParams]
})
export class EmergencyReportPageModule {}
