import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController, Platform } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-emergency-response',
  templateUrl: './emergency-response.page.html',
  styleUrls: ['./emergency-response.page.scss'],
})
export class EmergencyResponsePage implements OnInit {

  emergencyresponseList: any;
  response:any;
  viewResponse: any;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
    ) { 
      this.emergencyresponseList = [
        {'id': 1, 'title': 'Kebakaran 16 sept 2018', 'description': 'Petugas pemadam akan segera datang ke tempat dalam waktu 15 menit, Tenant diharapkan segera turun dan berkumpul di titik evakuasi'},
      ];
    }

    ngOnInit(){
      this.viewResponse = [];
      this.getdata();
    }
  
    getdata(){
       this.loading.present("Please wait...");
       this.storage.get('currentUser').then((storage: any)=> {
         this.api.getData('emergency/report/list/'+storage.id).subscribe((data)=> {
          this.response = data;
          this.loading.dismiss();
        });
       });
     }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad EmergencyResponsePage');
    }
    goToHome() {
      this.navCtrl.navigateRoot('home');
    }
  
    show(id: any) {
      this.viewResponse = [];
      this.viewResponse[id] = true;
    }

  back() {
    this.navCtrl.back();
  }

}
