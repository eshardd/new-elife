import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmergencyResponsePage } from './emergency-response.page';

describe('EmergencyResponsePage', () => {
  let component: EmergencyResponsePage;
  let fixture: ComponentFixture<EmergencyResponsePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergencyResponsePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmergencyResponsePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
