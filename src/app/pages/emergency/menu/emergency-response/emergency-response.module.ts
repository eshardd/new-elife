import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmergencyResponsePageRoutingModule } from './emergency-response-routing.module';

import { EmergencyResponsePage } from './emergency-response.page';
import { NavParams } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmergencyResponsePageRoutingModule
  ],
  declarations: [EmergencyResponsePage],
  providers:[NavParams]
})
export class EmergencyResponsePageModule {}
