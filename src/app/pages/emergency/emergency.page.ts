import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-emergency',
  templateUrl: './emergency.page.html',
  styleUrls: ['./emergency.page.scss'],
})
export class EmergencyPage implements OnInit {

  menus:any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
      {
        'col':[
        {'title': 'Emergency Button', 'page': 'emergency-button', 'img': 'sub-menu/emergency/emergency-button.png'},
        {'title': 'Emergency Report', 'page': 'emergency-report', 'img': 'sub-menu/emergency/emergency-report.png'},
        {'title': 'Emergency Response', 'page': 'emergency-response', 'img': 'sub-menu/emergency/emergency-response.png'},
        ]
      }
      ];
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  back() {
    this.navCtrl.back();
  }
}
