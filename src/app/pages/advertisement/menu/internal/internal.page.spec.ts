import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InternalPage } from './internal.page';

describe('InternalPage', () => {
  let component: InternalPage;
  let fixture: ComponentFixture<InternalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InternalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
