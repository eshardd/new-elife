import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InternalPageRoutingModule } from './internal-routing.module';

import { InternalPage } from './internal.page';
import { NavParams } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InternalPageRoutingModule
  ],
  declarations: [InternalPage],
  providers:[NavParams]
})
export class InternalPageModule {}
