import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InternalShowPage } from './internal-show.page';

describe('InternalShowPage', () => {
  let component: InternalShowPage;
  let fixture: ComponentFixture<InternalShowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalShowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InternalShowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
