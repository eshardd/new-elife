import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InternalShowPageRoutingModule } from './internal-show-routing.module';

import { InternalShowPage } from './internal-show.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InternalShowPageRoutingModule
  ],
  declarations: [InternalShowPage]
})
export class InternalShowPageModule {}
