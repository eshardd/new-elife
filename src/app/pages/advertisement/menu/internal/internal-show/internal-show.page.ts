import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../../../../services/api/api.service';
import { LoadingService } from '../../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-internal-show',
  templateUrl: './internal-show.page.html',
  styleUrls: ['./internal-show.page.scss'],
})
export class InternalShowPage implements OnInit {

  title: any = '';
 	images: any = '';
  constructor(
    public navCtrl	: NavController, 
		public params: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
  ) {
  }

  ngOnInit() {
    this.title = this.params.data.title;
		//this.images = '/assets/imgs/waskita/content/'+this.params.data.images;
    this.images = this.params.data.image;
  }

  close(){
    this.modal.dismiss();
  }
}
