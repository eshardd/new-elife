import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { InternalShowPage } from './internal-show/internal-show.page';

@Component({
  selector: 'app-internal',
  templateUrl: './internal.page.html',
  styleUrls: ['./internal.page.scss'],
})
export class InternalPage implements OnInit {

  slider: any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
    public loading: LoadingService,
    private modal:ModalController,
  ) { }

  ngOnInit() {
    this.getAdvertiseInternal();
    console.log('ionViewDidLoad InternalPage');
  }

  getAdvertiseInternal() {
		this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any) => {
			this.api.getData('advertisment/internal/' + storage.site).subscribe((data: any) => {
        this.slider = data;
        this.loading.dismiss();
			});
		});
  }
  
  async show(desc, content){
   const modal = await this.modal.create({
     component: InternalShowPage,
     componentProps: {
       'title': desc,
       'image': content
     },
     });
     return await modal.present();
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }
  back() {
    this.navCtrl.back();
  }
  home() {
    this.navCtrl.navigateRoot('home');
  }

}
