import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicServicePage } from './public-service.page';

describe('PublicServicePage', () => {
  let component: PublicServicePage;
  let fixture: ComponentFixture<PublicServicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicServicePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicServicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
