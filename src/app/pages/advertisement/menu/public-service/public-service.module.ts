import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicServicePageRoutingModule } from './public-service-routing.module';

import { PublicServicePage } from './public-service.page';
import { NavParams } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicServicePageRoutingModule
  ],
  declarations: [PublicServicePage],
  providers:[NavParams]
})
export class PublicServicePageModule {}
