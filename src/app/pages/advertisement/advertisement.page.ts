import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.page.html',
  styleUrls: ['./advertisement.page.scss'],
})
export class AdvertisementPage implements OnInit {

  menus: any;
  constructor(
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.menus = [
    {
      'col':[
      //{'title': 'External', 'page': 'ExternalPage', 'img': 'sub-menu/advertisement/external.png'},
      {'title': 'Internal', 'page': 'internal', 'img': 'sub-menu/advertisement/internal.png'},
      {'title': 'Public Service', 'page': 'public-service', 'img': 'sub-menu/advertisement/public-service.png'},
      {'title': 'Tenants', 'page': 'tenants', 'img': 'sub-menu/advertisement/tenants.png'}
      ]
    }
    ];
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  back() {
    this.navCtrl.back();
  }

}
