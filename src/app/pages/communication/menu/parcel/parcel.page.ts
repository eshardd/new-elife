import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-parcel',
  templateUrl: './parcel.page.html',
  styleUrls: ['./parcel.page.scss'],
})
export class ParcelPage implements OnInit {

  parcelList: any;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit(){
		this.storage.get('currentUser').then((storage: any)=> {
			this.getdata(storage.id);
		});
	}

	getdata(user_id:number){
		this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any)=> {
			this.api.getData('communication/parcel/list/'+user_id).subscribe((data: any)=> {
        this.parcelList = data;
        this.loading.dismiss();
		   });
		});
	}

	back(){
		this.navCtrl.back();
	}

  	home(){
		this.navCtrl.navigateRoot('home');
	}
}
