import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParcelPage } from './parcel.page';

describe('ParcelPage', () => {
  let component: ParcelPage;
  let fixture: ComponentFixture<ParcelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParcelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParcelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
