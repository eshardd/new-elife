import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { ProcessDisputePage } from '../../../../components/process-dispute/process-dispute.page';
import { InvoiceViewPage } from '../../../../components/invoice-view/invoice-view.page';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.page.html',
  styleUrls: ['./guest.page.scss'],
})
export class GuestPage implements OnInit {

  security: any = 'messages';
 	
	tenant_name:string;
 	response: any;
 	Form : FormGroup;
 	tempdata:any;

 	guessList;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) {
    this.Form = this.formBuilder.group({
			tenant_id		: 	[''],
			messages		: 	['', Validators.required],
		});
  }

  ngOnInit(){
    this.storage.get('currentUser').then((storage: any)=> {
      this.getdata(storage.id);
    });
  }

  getdata(user_id:number){
    this.loading.present("Please wait...");
    this.storage.get('currentUser').then((storage: any)=> {
      this.api.getData('communication/guest/list/'+user_id).subscribe((data: any)=> {
        this.guessList = data;
        this.loading.dismiss();
     });
    });
  }

  async reject(row_id:number) {
    let prompt = await this.alertCtrl.create({
      subHeader: 'Reject',
      message: "Enter your Notes and we'll help you",
      inputs: [
      {
        name: 'reject_notes',
        placeholder: 'Email'
      },
      ],
      buttons: [
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Send',
        handler: data => {
          data.id = row_id;
          this.rejectGuest(data);
        }
      }
      ]
    });
    await prompt.present();
  }

  accept(row_id:number) {
    var data = {
      'id':row_id,
      'action':1
    };
    
    this.loading.present("Please wait...");
    this.api.updateData('communication/guest/accept',data).subscribe((data)=> {
     if (data != undefined) {
       this.toastr.present("Success",'top');
       this.loading.dismiss();
       this.navCtrl.pop();
     }
   });
  }

  rejectGuest(data:any){
    this.loading.present("Please wait...");
    data.action = 2;

   this.api.updateData('communication/guest/reject',data).subscribe((data)=> {
     if (data != undefined) {
       this.toastr.present("Success","top");
       this.loading.dismiss();
       this.navCtrl.pop();
     }
   });
  }

  process(){
    /*let loader = this.loadingCtrl.create({
     content: "Please wait..."
   });
   loader.present();*/
   this.storage.get('currentUser').then((storage: any)=> {
     this.Form.get('tenant_id').setValue(storage.id);
     console.log(this.Form.value);
     /*this.api.postData('service_desk/add',this.Form.value).subscribe((data)=> {
       if (data != undefined) {
         let toast = this.toastCtrl.create({
           message: "Success",
           duration: 3000,
           position: 'top'
         });
         toast.present();
         loader.dismiss();
         this.navCtrl.push('ServiceDeskPage');
       }
     });*/
   });
  }

  back(){
		this.navCtrl.back();
	}

  home(){
		this.navCtrl.navigateRoot('home');
	}
}
