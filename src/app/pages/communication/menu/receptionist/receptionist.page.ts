import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-receptionist',
  templateUrl: './receptionist.page.html',
  styleUrls: ['./receptionist.page.scss'],
})
export class ReceptionistPage implements OnInit {

  tenant_name:string;
 	receptionist: any = 'messages';
  response: any;
 	Form : FormGroup;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) {
    this.Form = this.formBuilder.group({
			tenant_id		: 	[''],
			menu			: 	[''],
			site			: 	[''],
			messages		: 	['', Validators.required],
		});
   }

   ngOnInit(){
    this.storage.get('currentUser').then((storage: any)=> {
      this.tenant_name = storage.name;
    });

    this.getdata();
  }

  getdata(){
    this.loading.present("Please wait...");
    this.storage.get('currentUser').then((storage: any)=> {
      this.api.getData('communication/receptionist/list/'+storage.id).subscribe((data)=> {
        this.response = data;
        this.response = this.response.filter( x => x['communication_menu'] == 1);
        this.loading.dismiss();
     });
    });
  }

  process(){
    this.loading.present("Please wait...");
     this.storage.get('currentUser').then((storage: any)=> {
     this.Form.get('tenant_id').setValue(storage.id);
     this.Form.get('menu').setValue("1");
     this.Form.get('site').setValue(storage.site);
     this.api.postData('communication/receptionist/add',this.Form.value).subscribe((data)=> {
       if (data != undefined) {
         this.toastr.present("Succes","top");
         this.navCtrl.pop();
       }
     });
     this.loading.dismiss();
   });
  }


  back(){
		this.navCtrl.back();
  }
  
  home(){
		this.navCtrl.navigateRoot('home');
	}

}
