import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReceptionistPageRoutingModule } from './receptionist-routing.module';

import { ReceptionistPage } from './receptionist.page';
import { NavParams } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ReceptionistPageRoutingModule
  ],
  declarations: [ReceptionistPage],
  providers:[NavParams]
})
export class ReceptionistPageModule {}
