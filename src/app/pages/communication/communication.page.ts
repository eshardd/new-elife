import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-communication',
  templateUrl: './communication.page.html',
  styleUrls: ['./communication.page.scss'],
})
export class CommunicationPage implements OnInit {
  menus: any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
      {
        'col':[
        {'title': 'Guess', 'page': 'guest', 'img': 'sub-menu/communication/guess.png'},
        {'title': 'Parcel', 'page': 'parcel', 'img': 'sub-menu/communication/parcel.png'},
        {'title': 'Receptionist', 'page': 'receptionist', 'img': 'sub-menu/communication/receptionist.png'},
        {'title': 'Security', 'page': 'security', 'img': 'sub-menu/communication/security.png'},
        ]
      }
      ];
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  back() {
    this.navCtrl.back();
  }
}
