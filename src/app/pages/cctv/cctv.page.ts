import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';

declare var window: any;

@Component({
  selector: 'app-cctv',
  templateUrl: './cctv.page.html',
  styleUrls: ['./cctv.page.scss'],
})
export class CctvPage implements OnInit {

  url: any = 'rtsp://svrcctv:cctvsvrn3tc1t1@10.3.3.9:554/Streaming/Channels/';
  //'rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov';


  cctv: any = [
    {
      'cctv': [
        { 'id': '101', 'title': 'CCTV 1', 'img': 'sub-menu/cctv/cctv1.png' },
        { 'id': '102', 'title': 'CCTV 2', 'img': 'sub-menu/cctv/cctv2.png' }
      ]
    }
  ];
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
  }

  play_cctv(id) {
    let path = this.url+id+'/';
    window.VideoPlayerVLC.play(
      path,
      done => { },
      error => { }
    );
  }

  back() {
    this.navCtrl.back();
  }

}
