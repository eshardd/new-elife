import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, PopoverController, Platform } from '@ionic/angular';
//import { NotifComponent } from '../../components/notif/notif';
import { Storage } from '@ionic/storage';
import { SQLite } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  slider: any;
	menus: any;
	showSearch: boolean = false;

  slideOpts = {
    initialSlide: 1,
    speed: 40
  };
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public popoverCtrl: PopoverController,
		private platform: Platform,
		private storage: Storage,
		private sqlite: SQLite
  ) { }

  ngOnInit() {
	this.storage.get('currentUser').then((storage: any)=> {
		console.log(storage);
	});
    this.slider = [
			{ 'title': 'Slider 1', 'description': 'Logo', 'url': '/assets/img/01.jpg' },
			{ 'title': 'Slider 2', 'description': 'News', 'url': '/assets/img/02.jpg' },
			//{'title':'Slider 3','description':'Panic Button','url': '/assets/img/panic-button.jpg'},
			{ 'title': 'Slider 4', 'description': 'Advertisment', 'url': '/assets/img/03.jpg' }
    ];
    
    this.menus = [
			{
				'col': [
					{ 'title': 'Administrasi', 'page': 'administration', 'img': 'profile.png' },
					{ 'title': 'Billing', 'page': 'billing', 'img': 'billing.png' },
					{ 'title': 'Facility', 'page': 'facility', 'img': 'facility.png' },
					{ 'title': 'Service Desk', 'page': 'service-desk', 'img': 'service-desk.png' }
				]
			},
			{
				'col': [
					{ 'title': 'Announcement', 'page': 'announcement', 'img': 'announcement.png' },
					{ 'title': 'Advertisement', 'page': 'advertisement', 'img': 'advertisment.png' },
					{ 'title': 'Order / Purchase', 'page': 'order', 'img': 'order.png' },
					{ 'title': 'Communication', 'page': 'communication', 'img': 'communication.png' }
				]
			},
			{
				'col': [
					{ 'title': 'CCTV', 'page': 'cctv', 'img': 'buttonmenu-cct_cctv.png' },
					{ 'title': 'Directory', 'page': 'directory', 'img': 'directory.png' },
					{ 'title': 'Emergency', 'page': 'emergency', 'img': 'emergency.png' },
					{ 'title': '&nbsp;', 'page': 'none', 'img': '' }
				]
			}
		];
  	}

  	goToPage(page: string) {
		this.navCtrl.navigateForward(page);
	}

	PanicButton() {
		this.navCtrl.navigateForward('emergency-button');
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad HomePage');
	}

	end() {
		return false;
	}


	logout() {
		this.storage.clear();
		this.platform.ready().then(() => {
			this.sqlite.deleteDatabase({
				name: 'elife.db',
				location: 'default'
			});
		});
		this.navCtrl.navigateRoot('login');
	}
}
