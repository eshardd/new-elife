import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-facility',
  templateUrl: './facility.page.html',
  styleUrls: ['./facility.page.scss'],
})
export class FacilityPage implements OnInit {

  menus:any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
    {
      'col':[
      {'title': 'Booking', 'page': 'booking', 'img': 'sub-menu/facility/booking.png'},
      {'title': 'Facility Order', 'page': 'facility-order', 'img': 'sub-menu/facility/facility_order.png'},
      {'title': 'Facility Payment', 'page': 'facility-payment', 'img': 'sub-menu/facility/facility_payment.png'},
      {'title': 'Schedule', 'page': 'facility-schedule', 'img': 'sub-menu/facility/schedule.png'}
      ]
    }
    ];
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }
  back() {
    this.navCtrl.back();
  }
}
