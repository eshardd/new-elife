import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { InvoiceViewPage } from '../../../../components/invoice-view/invoice-view.page';
import { ProcessFacilityPaymentPage } from '../../../../components/process-facility-payment/process-facility-payment.page';

@Component({
  selector: 'app-facility-payment',
  templateUrl: './facility-payment.page.html',
  styleUrls: ['./facility-payment.page.scss'],
})
export class FacilityPaymentPage implements OnInit {

  facilityPayment: string = 'outstanding';
	outstanding: any;
	paid: any;

	outstandinglist: any;
  paidlist: any;
  
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
    public loading: LoadingService,
    private modal			:ModalController,
  ) { }

  ngOnInit() {
		this.getdata();
	}

	async payment(id: any) {
		const modal = await this.modal.create({
		  component: ProcessFacilityPaymentPage,
		  componentProps: {
			  'id': id,
		  },
		});
		return await modal.present();
	}

	getdata() {
		this.storage.get('currentUser').then((storage: any) => {
      this.loading.present("Please wait...");
			this.api.getData('facility/facility_payment/'+storage.id).subscribe((data: any) => {
				this.outstandinglist = data.filter(item => item['status'] != 2);
				this.paidlist = data.filter(item => item['status'] == 2);
			}, (error: any) => { }, () => {
				this.loading.dismiss()
			});
		});
	}

	async viewinvoice(invoice:any){
   const modal = await this.modal.create({
     component: InvoiceViewPage,
     componentProps: {
       'invoice': invoice,
     },
     });
     return await modal.present();
  }


	ionViewDidLoad() {
		console.log('ionViewDidLoad FacilityPaymentPage');
	}

	back() {
		this.navCtrl.back();
	}
	
	home() {
		this.navCtrl.navigateRoot('home');
	}

}
