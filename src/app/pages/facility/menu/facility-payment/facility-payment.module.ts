import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacilityPaymentPageRoutingModule } from './facility-payment-routing.module';

import { FacilityPaymentPage } from './facility-payment.page';

import { NavParams } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    FacilityPaymentPageRoutingModule
  ],
  declarations: [FacilityPaymentPage],
  providers:[NavParams]
})
export class FacilityPaymentPageModule {}
