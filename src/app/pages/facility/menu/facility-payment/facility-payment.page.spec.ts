import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacilityPaymentPage } from './facility-payment.page';

describe('FacilityPaymentPage', () => {
  let component: FacilityPaymentPage;
  let fixture: ComponentFixture<FacilityPaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityPaymentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacilityPaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
