import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacilityPaymentPage } from './facility-payment.page';

const routes: Routes = [
  {
    path: '',
    component: FacilityPaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacilityPaymentPageRoutingModule {}
