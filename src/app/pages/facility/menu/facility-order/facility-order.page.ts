import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-facility-order',
  templateUrl: './facility-order.page.html',
  styleUrls: ['./facility-order.page.scss'],
})
export class FacilityOrderPage implements OnInit {

  facilities: any;
	orderForm: FormGroup;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) {
    this.orderForm = this.formBuilder.group({
			id_tenant: [''],
			date: ['', Validators.required],
			facility: ['', Validators.required],
			amount: [''],
		});
  }

  ngOnInit() {
		this.getdatafacility();
	}

	getdatafacility() {
		this.storage.get('currentUser').then((storage: any) => {
			this.loading.present('Please wait...');
			this.api.getData('facility/all/'+storage.site).subscribe((data: any) => {
				this.facilities = data;
			}, (error: any) => { }, () => {
				this.loading.dismiss();
			});
		});
	}

	submitForm() {
		this.loading.present('Please wait...');

		this.storage.get('currentUser').then((storage: any) => {
			let price = this.getprice(this.orderForm.value.facility);
			this.orderForm.get('id_tenant').setValue(storage.id);
			this.orderForm.get('amount').setValue(price);
			this.api.postData('facility/order', this.orderForm.value).subscribe((data) => {
				if (data['id'] != undefined) {
					this.toastr.present("Success",'top');
				}
			}, (error: any) => { }, () => {
				this.loading.dismiss();
				this.navCtrl.back();
			});
		});
	}

	getprice(facility_id: number) {
		let a = this.facilities.find(x => x.id === facility_id);
		return (a.price);
	}


	back() {
		this.navCtrl.back();
	}

	home() {
		this.navCtrl.navigateRoot('home');
	}

}
