import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacilityOrderPage } from './facility-order.page';

const routes: Routes = [
  {
    path: '',
    component: FacilityOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacilityOrderPageRoutingModule {}
