import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacilityOrderPage } from './facility-order.page';

describe('FacilityOrderPage', () => {
  let component: FacilityOrderPage;
  let fixture: ComponentFixture<FacilityOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacilityOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
