import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacilityOrderPageRoutingModule } from './facility-order-routing.module';

import { FacilityOrderPage } from './facility-order.page';
import { NavParams } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    FacilityOrderPageRoutingModule
  ],
  declarations: [FacilityOrderPage],
  providers:[NavParams]
})
export class FacilityOrderPageModule {}
