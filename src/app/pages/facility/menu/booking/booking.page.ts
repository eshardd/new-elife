import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
//import { ProcessDisputeComponent } from '../../../../components/process-dispute/process-dispute';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.page.html',
  styleUrls: ['./booking.page.scss'],
})
export class BookingPage implements OnInit {

  facilities: any;
	bookingForm: FormGroup;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) {
    this.bookingForm = this.formBuilder.group({
			id_tenant: [''],
			date: ['', Validators.required],
			facility: ['', Validators.required]
		});
  }

  ngOnInit() {
		this.getdatafacility();
	}

	getdatafacility() {
		this.storage.get('currentUser').then((storage: any) => {
      this.loading.present("Please wait...");
			this.api.getData('facility/all/'+storage.site).subscribe((data: any) => {
				this.facilities = data;
			}, (error: any) => { }, () => {
				this.loading.dismiss();
			});
		});
	}

	submitForm() {
		this.loading.present('Please wait...');
		this.storage.get('currentUser').then((storage: any) => {
			this.bookingForm.get('id_tenant').setValue(storage.id);
			this.api.postData('facility/booking', this.bookingForm.value).subscribe((data) => {
				if (data['id'] != undefined) {
					this.toastr.present(data['message'],'top');
				}
			}, (error: any) => { }, () => {
				this.loading.dismiss()
        this.navCtrl.back();
			});
		});
	}

	back() {
		this.navCtrl.back();
	}
	
	home() {
		this.navCtrl.navigateRoot('home');
	}

}
