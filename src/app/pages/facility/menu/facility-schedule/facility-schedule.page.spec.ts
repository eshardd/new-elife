import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacilitySchedulePage } from './facility-schedule.page';

describe('FacilitySchedulePage', () => {
  let component: FacilitySchedulePage;
  let fixture: ComponentFixture<FacilitySchedulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitySchedulePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacilitySchedulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
