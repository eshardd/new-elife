import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacilitySchedulePageRoutingModule } from './facility-schedule-routing.module';

import { FacilitySchedulePage } from './facility-schedule.page';
import { NavParams } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    FacilitySchedulePageRoutingModule
  ],
  declarations: [FacilitySchedulePage],
  providers:[NavParams]
})
export class FacilitySchedulePageModule {}
