import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-facility-schedule',
  templateUrl: './facility-schedule.page.html',
  styleUrls: ['./facility-schedule.page.scss'],
})
export class FacilitySchedulePage implements OnInit {

  facility: any = 'booking';
	booking: any;
  order: any;
	datakey: any;

  scheduleList: any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) { }

  ngOnInit(){
    this.loading.present("Please wait...");
    
    this.booking = [];
  	this.api.getData('schedule/all').subscribe((data: any)=> {

      this.booking = Object.keys(data.booking).map(key => ({date: key, value: data.booking[key]}));
      this.order = Object.keys(data.order).map(key => ({date: key, value: data.order[key]}));
      this.loading.dismiss();
 	});
  }

  back() {
		this.navCtrl.back();
	}

  home() {
		this.navCtrl.navigateRoot('home');
	}

}
