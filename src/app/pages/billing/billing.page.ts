import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.page.html',
  styleUrls: ['./billing.page.scss'],
})
export class BillingPage implements OnInit {

  menus:any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
    {
      'col':[
        {'title': 'Dispute', 'page': 'dispute', 'img': 'sub-menu/billing/dispute.png'},
        {'title': 'Invoice', 'page': 'invoice', 'img': 'sub-menu/billing/invoice.png'}
      ]
    },
    {
      'col':[
        {'title': 'Payment', 'page': 'payment', 'img': 'sub-menu/billing/payment.png'}
      ]
    }
    ];
  }


  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  back() {
    this.navCtrl.back();
  }
}
