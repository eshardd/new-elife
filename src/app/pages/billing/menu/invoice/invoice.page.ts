import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { ProcessDisputePage } from '../../../../components/process-dispute/process-dispute.page';
import { InvoiceViewPage } from '../../../../components/invoice-view/invoice-view.page';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
//import { DocumentViewer } from '@ionic-native/document-viewer/ngx';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.page.html',
  styleUrls: ['./invoice.page.scss'],
})
export class InvoicePage implements OnInit {

  public invoice: any;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
		private file: File,
		private transfer: FileTransfer,
		private fileOpener: FileOpener,
  ) { }

  ngOnInit(){
		this.storage.get('currentUser').then((storage: any)=> {
			this.getdatainvoice(storage.id);
		});
	}

   	getdatainvoice(tenant_id:number){
   		this.loading.present("Please wait...");

   		this.api.getData('invoice/'+tenant_id).subscribe((data: any)=> {
	      //this.invoice = data;
	      this.invoice = Object.keys(data).map(key => ({date: key, value: data[key].data, subtotal:data[key].subtotal}));
	      this.loading.dismiss();
	    });
   	}

   	download() {
		const fileTransfer: FileTransferObject = this.transfer.create();
		const url = 'http://www.pdf995.com/samples/pdf.pdf';

		// let loader = this.loadingCtrl.create({
	  	// 	content: "Processing dowload ivoice ..."
	 	// });
		// loader.present();

		fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry: any) => {
			this.fileOpener.open(entry.toURL(), 'application/pdf')
			.then(() => console.log('a'))
			.catch(e => console.log('Error opening file', e));
		}, (error) => {
	 	});
   	}

	// presentAlert() {
	// 	let alert = this.alertCtrl.create({
	// 		title: 'Low battery',
	// 		subTitle: '10% of battery remaining',
	// 		buttons: ['Dismiss']
	// 	});
	// 	alert.present();
	// }

	async openmodal(invoice:any){
		// let myModal = this.modal.create('InvoiceViewPage', {'invoice': invoice.id});
    // myModal.present();
    const modal = await this.modal.create({
			component: InvoiceViewPage,
			componentProps: {
				'invoice': invoice.id,
			},
		  });
		  return await modal.present();
	}

	home() {
		this.navCtrl.navigateRoot('home');
  }
  
  back() {
		this.navCtrl.back();
	}

}
