import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { ProcessPaymentPage } from '../../../../components/process-payment/process-payment.page';
import { InvoiceViewPage } from '../../../../components/invoice-view/invoice-view.page';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  payment_toolbar: string = 'outstanding';
	paymentForm : FormGroup;
	myPhoto: any;
	outstandinglist: any;
	paidlist: any;
	disputelist: any;

  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
  ) { 
    this.paymentForm = this.formBuilder.group({
			tenant_id			: 	'',
			invoice				: 	['', Validators.required],
			payment_date		: 	['', Validators.required],
			bank_account_name	: 	['', Validators.required],
			bank_account_number	: 	['', Validators.required],
			amount				: 	['', Validators.required],
		});
  }


  ngOnInit(){
		this.getinvoice();
		//this.getdropdown();
	}

	getinvoice(){
		this.loading.present('Please wait...');
		this.storage.get('currentUser').then((storage: any)=> {
			this.api.getData('billing/payment/invoice/'+storage.id).subscribe((data: any)=> {
				this.outstandinglist = data.invoice.filter(item => item['status'] != 2);
				console.log(this.outstandinglist);
				this.paidlist = data.invoice.filter(item => item['status'] == 2);
				//this.disputelist = data.dispute;
				this.loading.dismiss();
			});
		});
	}

	async viewinvoice(invoice:any){
   const modal = await this.modal.create({
     component: InvoiceViewPage,
     componentProps: {
       'invoice': invoice,
     },
     });
     return await modal.present();
  }

  async payment(id:any){;
   const modal = await this.modal.create({
     component: ProcessPaymentPage,
     componentProps: {
       'id': id,
     },
     });
     return await modal.present();
  }



   back(){
		this.navCtrl.back();
	}

	home(){
		this.navCtrl.navigateRoot('home');
	}
}
