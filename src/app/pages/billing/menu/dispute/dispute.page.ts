import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { ProcessDisputePage } from '../../../../components/process-dispute/process-dispute.page';
import { InvoiceViewPage } from '../../../../components/invoice-view/invoice-view.page';


@Component({
  selector: 'app-dispute',
  templateUrl: './dispute.page.html',
  styleUrls: ['./dispute.page.scss'],
})
export class DisputePage implements OnInit {

  payment_toolbar: string = 'invoice';
	disputeForm : FormGroup;
	dispute_type:any;
	invoice_list:any;
  dispute_list:any;
  
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
  ) {
    this.disputeForm = this.formBuilder.group({
			tenant_id		: 	'',
			invoice			: 	['', Validators.required],
			type			: 	['', Validators.required],
			description		: 	['', Validators.required],
		});
  }

  ngOnInit() {
    this.getinvoice();
  }

  getinvoice(){
    this.loading.present('Please wait...');
		this.storage.get('currentUser').then((storage: any)=> {
			this.api.getData('billing/dispute/invoice/'+storage.id).subscribe((data: any)=> {
				this.invoice_list = data.invoice;
				this.dispute_list = data.dispute;
				this.loading.dismiss();
				console.log(data);
			});
		});
	}

	async viewinvoice(invoice:any){
 		// let myModal = this.modal.create('InvoiceViewPage', {'invoice': invoice});
		// myModal.present();
		const modal = await this.modal.create({
			component: InvoiceViewPage,
			componentProps: {
				'invoice': invoice,
			},
		  });
		  return await modal.present();
 	}

   async dispute(id: any) {
		const modal = await this.modal.create({
		  component: ProcessDisputePage,
		  componentProps: {
			  'id': id,
		  },
		});
		return await modal.present();
	}


	back(){
		this.navCtrl.back();
	}

	home(){
		this.navCtrl.navigateRoot('home');
	}
}
