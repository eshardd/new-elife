import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DisputePageRoutingModule } from './dispute-routing.module';

import { DisputePage } from './dispute.page';
import { NavParams } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DisputePageRoutingModule
  ],
  declarations: [DisputePage],
  providers:[NavParams]
})
export class DisputePageModule {}
