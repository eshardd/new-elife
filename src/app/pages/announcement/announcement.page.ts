import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.page.html',
  styleUrls: ['./announcement.page.scss'],
})
export class AnnouncementPage implements OnInit {

  menus:any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
    {
      'col':[
      {'title': 'General', 'page': 'general', 'img': 'sub-menu/announcement/general.png'},
      {'title': 'Government', 'page': 'government', 'img': 'sub-menu/announcement/goverment.png'},
      {'title': 'Holidays', 'page': 'holidays', 'img': 'sub-menu/announcement/holidays.png'},
      {'title': 'Maintenance', 'page': 'maintenance', 'img': 'sub-menu/announcement/maintenance.png'},
      {'title': 'Public', 'page': 'public', 'img': 'sub-menu/announcement/public.png'},
      ]
    }
    ];
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }
  back() {
    this.navCtrl.back();
  }


}
