import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.page.html',
  styleUrls: ['./holidays.page.scss'],
})
export class HolidaysPage implements OnInit {

  holidayAnnouncement: any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) { }

  ngOnInit() {
		this.getAnnouncementHolidays();
		console.log('ionViewDidLoad GovernmentPage');
	}

	getAnnouncementHolidays() {
		this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any) => {
			this.api.getData('announcement/holidays/' + storage.site).subscribe((data: any) => {
				this.holidayAnnouncement = data;
				this.loading.dismiss();
			});
		});
	}

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  home() {
		this.navCtrl.navigateRoot('home');
  }

  back() {
    this.navCtrl.back();
  }

}
