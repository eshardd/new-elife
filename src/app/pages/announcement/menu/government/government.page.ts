import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-government',
  templateUrl: './government.page.html',
  styleUrls: ['./government.page.scss'],
})
export class GovernmentPage implements OnInit{

  govermentAnnouncement: any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) { }

  ngOnInit() {
		this.getAnnouncementGoverment();
		console.log('ionViewDidLoad GovernmentPage');
	}

	getAnnouncementGoverment() {
		this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any) => {
			this.api.getData('announcement/goverment/' + storage.site).subscribe((data: any) => {
				this.govermentAnnouncement = data;
				this.loading.dismiss();
			});
		});
	}

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  home() {
		this.navCtrl.navigateRoot('home');
  }

  back() {
    this.navCtrl.back();
  }

}
