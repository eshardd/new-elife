import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-general',
  templateUrl: './general.page.html',
  styleUrls: ['./general.page.scss'],
})
export class GeneralPage implements OnInit {

  generalAnnouncement: any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) { }

  ngOnInit() {
    this.getAnnouncementGeneral();
  }

  getAnnouncementGeneral() {
    this.loading.present('Please wait...');
		this.storage.get('currentUser').then((storage: any) => {
			this.api.getData('announcement/general/' + storage.site).subscribe((data: any) => {
				this.generalAnnouncement = data;
				this.loading.dismiss();
			});
		});
	}

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }
  back() {
    this.navCtrl.back();
  }

  home() {
    this.navCtrl.navigateRoot('home');
  }

}
