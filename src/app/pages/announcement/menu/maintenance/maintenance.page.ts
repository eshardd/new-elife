import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.page.html',
  styleUrls: ['./maintenance.page.scss'],
})
export class MaintenancePage implements OnInit {

  maintenanceAnnouncement: any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) { }

  ngOnInit() {
		this.getAnnouncementMaintenance();
		console.log('ionViewDidLoad GovernmentPage');
	}

	getAnnouncementMaintenance() {
		this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any) => {
			this.api.getData('announcement/maintenance/' + storage.site).subscribe((data: any) => {
				this.maintenanceAnnouncement = data;
				this.loading.dismiss();
			});
		});
	}

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  home() {
		this.navCtrl.navigateRoot('home');
  }

  back() {
    this.navCtrl.back();
  }

}
