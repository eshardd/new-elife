import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { ApiService } from '../../../services/api/api.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { ToastrService } from '../../../services/toastr/toastr.service';
import { Storage } from '@ionic/Storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {

	loginForm : FormGroup;
	constructor(
    	public api: ApiService,
 		public navCtrl: NavController,
 		//public navParams: NavParams,
 		//public toastCtrl: ToastController,
 		//public actionSheetCtrl: ActionSheetController,
 		public alertCtrl: AlertController,
		 //public loadingCtrl: LoadingController,
		public toastr: ToastrService,
 		public loading: LoadingService,
 		private storage: Storage,
 		private formBuilder: FormBuilder,
 		//private menuCtrl: MenuController,
		private sqlite: SQLite,
 		private platform: Platform
	) { 
    	//this.menuCtrl.swipeEnable(false);
 		this.loginForm = this.formBuilder.group({
 			email: 		['', Validators.email],
 			password: 	['', Validators.required]
 		});
  	}

	ngOnInit() {
		this.platform.ready().then(() => {
			if (this.platform.is('cordova')) {
				this.sqlite.create({
					name: 'elife.db',
					location: 'default'
				})
				.then((db: SQLiteObject) => {
					db.executeSql('SELECT * FROM elife', [])
					.then((results) => {
						if(results.rows.length > 0) {
							console.log(results.rows.item(0).data);
							let data = JSON.parse(results.rows.item(0).data);
							this.storage.set('currentUser', data);
							this.navCtrl.navigateRoot('HomePage');
						}
					})
					.catch(e => {
						console.log('no data in database');
					});
				})
				.catch(e => console.log(e));
			}
		});
	}

	logForm() {
		this.loading.present("Please wait...");
		this.api.postData('login',this.loginForm.value).subscribe((data)=> {
			if (data['data'] != undefined) {
				this.loading.dismiss();
				this.storage.set('currentUser', data['data']);
				this.loginForm.reset();
				this.navCtrl.navigateRoot('home');
			}else{
				this.loading.dismiss();
				this.toastr.present(data['message'],'middle');
				this.loginForm.reset();
			}
		});
	}

	createDb(token: string, data: any) {
		this.platform.ready().then(() => {
			if (this.platform.is('cordova')) {
				this.sqlite.create({
					name: 'elife.db',
					location: 'default'
				})
				.then((db: SQLiteObject) => {
					let sql = 'CREATE TABLE IF NOT EXISTS `elife` (token VARCHAR(255), data TEXT)';
					db.executeSql(sql, [])
					.then(() => {
						console.log('Database created successfully');
					})
					.catch(e =>{
						console.log('Database created failed');
					});

					db.executeSql('INSERT INTO elife VALUES(?, ?) ', [token, JSON.stringify(data)])
					.then(() => {
						console.log('Insert database successfully');
					})
					.catch(e =>{
						console.log('Insert database failed');
					});
				})
				.catch(e => console.log(e));
			}
		});
	}

	async forgotPassword() {
		// let prompt = this.alertCtrl.create({
		// 	title: 'Forgot Password',
		// 	message: "Enter your email address and we'll help you reset your password.",
		// 	inputs: [
		// 	{
		// 		name: 'email',
		// 		placeholder: 'Email'
		// 	},
		// 	],
		// 	buttons: [
		// 	{
		// 		text: 'Cancel',
		// 		handler: data => {
		// 			console.log('Cancel clicked');
		// 		}
		// 	},
		// 	{
		// 		text: 'Send',
		// 		handler: data => {
		// 			this.resetPassword(data);
		// 			console.log(data);
		// 			console.log('Saved clicked');
		// 		}
		// 	}
		// 	]
		// });
		// prompt.present();
		const alert = await this.alertCtrl.create({
			cssClass: 'my-custom-class',
			subHeader: 'Forgot Password',
			message: "Enter your email address and we'll help you reset your password.",
				inputs: [
				{
					name: 'email',
					placeholder: 'Email'
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: data => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Send',
					handler: data => {
						this.resetPassword(data);
						console.log(data);
						console.log('Saved clicked');
					}
				}
			]
		  });
	
		await alert.present();
	}

	resetPassword(email: any) {
		this.loading.present('Please wait...');
		this.api.postData('forgotpassword', email).subscribe((data)=> {
			if (data['data'] != undefined) {
				this.loading.dismiss();
				this.toastr.present("Success",'middle');
				this.navCtrl.navigateRoot('login');
			}else{
				this.loading.dismiss();
				this.toastr.present('Email not exist','middle');
			}
		});

	}

  	goTo(page){
		this.navCtrl.navigateForward(page);
	}

}
