import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-service-desk',
  templateUrl: './service-desk.page.html',
  styleUrls: ['./service-desk.page.scss'],
})
export class ServiceDeskPage implements OnInit {

  menus:any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
    {
      'col':[
      {'title': 'Complaint', 'page': 'complaint', 'img': 'sub-menu/service-desk/complaint.png'},
      {'title': 'Incident', 'page': 'incident', 'img': 'sub-menu/service-desk/incident.png'},
      {'title': 'Report', 'page': 'report', 'img': 'sub-menu/service-desk/report.png'},
      {'title': 'Request', 'page': 'request', 'img': 'sub-menu/service-desk/request.png'},
      {'title': 'Suggestion', 'page': 'suggestion', 'img': 'sub-menu/service-desk/suggestion.png'},
      {'title': 'Work Order', 'page': 'work-order', 'img': 'sub-menu/service-desk/work-order.png'},
      ]
    }
    ];
  }
  
  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  back() {
    this.navCtrl.back();
  }

}
