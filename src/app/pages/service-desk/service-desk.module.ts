import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServiceDeskPageRoutingModule } from './service-desk-routing.module';

import { ServiceDeskPage } from './service-desk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ServiceDeskPageRoutingModule
  ],
  declarations: [ServiceDeskPage]
})
export class ServiceDeskPageModule {}
