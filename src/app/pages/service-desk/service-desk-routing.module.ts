import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServiceDeskPage } from './service-desk.page';

const routes: Routes = [
  {
    path: '',
    component: ServiceDeskPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceDeskPageRoutingModule {}
