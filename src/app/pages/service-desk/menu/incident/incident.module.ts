import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IncidentPageRoutingModule } from './incident-routing.module';

import { IncidentPage } from './incident.page';
import { NavParams } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    IncidentPageRoutingModule
  ],
  declarations: [IncidentPage],
  providers:[NavParams]
})
export class IncidentPageModule {}
