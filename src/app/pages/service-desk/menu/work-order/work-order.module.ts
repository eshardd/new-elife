import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorkOrderPageRoutingModule } from './work-order-routing.module';

import { WorkOrderPage } from './work-order.page';
import { NavParams } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    WorkOrderPageRoutingModule
  ],
  declarations: [WorkOrderPage],
  providers:[NavParams]
})
export class WorkOrderPageModule {}
