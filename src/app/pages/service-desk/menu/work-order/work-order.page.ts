import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-work-order',
  templateUrl: './work-order.page.html',
  styleUrls: ['./work-order.page.scss'],
})
export class WorkOrderPage implements OnInit {

  Form : FormGroup;
  tenant_name: string;

  service_desk_menu : string = "2";
  service_type : any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) {
    this.Form = this.formBuilder.group({
			tenant_id		: 	[''],
			name			: 	[''],
			menu			: 	[''],
			type			: 	['', Validators.required],
			messages		: 	['', Validators.required],
		});
  }

  ngOnInit(){
    this.storage.get('currentUser').then((storage: any)=> {
      this.tenant_name = storage.name;
    });

    this.getdropdown();
  }

  getdropdown(){
    this.loading.present("Please wait...");
   
   this.api.getData('service_desk/dropdown').subscribe((data: any)=> {
     this.service_type = data.filter( x => x['menu'] === this.service_desk_menu);
     this.loading.dismiss();
   });
   
 }

 process(){
    this.loading.present("Please wait...");
    this.storage.get('currentUser').then((storage: any)=> {
     this.Form.get('tenant_id').setValue(storage.id);
     this.Form.get('menu').setValue(parseInt(this.service_desk_menu));
     this.api.postData('service_desk/add',this.Form.value).subscribe((data)=> {
       if (data != undefined) {
         this.toastr.present("Success","top");
         this.loading.dismiss();
         this.navCtrl.back();
       }
     });
   });
  }

  back() {
		this.navCtrl.back();
	}

  home() {
		this.navCtrl.navigateRoot('home');
	}

}
