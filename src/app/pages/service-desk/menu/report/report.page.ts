import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

  data		: any;
 	segment		: any = 'complaint';
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
		public loading: LoadingService,
  ) { }

  ngOnInit() {
    this.data = [];
    this.loading.present("Please wait...");
    this.storage.get('currentUser').then((storage: any)=> {
      this.api.getData('service_desk/report_list/'+storage.id).subscribe((data: any)=>{
        this.data['complaint'] 	= data.filter( x => x['menu'] == 1);
        this.data['work_order'] = data.filter( x => x['menu'] == 2);
        this.data['incident'] 	= data.filter( x => x['menu'] == 3);
        this.data['request'] 	= data.filter( x => x['menu'] == 4);
        this.data['suggestion'] = data.filter( x => x['menu'] == 5);
        this.loading.dismiss();
      }, (error: any)=>{

      }, ()=>{
        //loader.dismiss();
      });
    });
  }

  back() {
		this.navCtrl.back();
	}

  home() {
		this.navCtrl.navigateRoot('home');
	}
}
