import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.page.html',
  styleUrls: ['./administration.page.scss'],
})
export class AdministrationPage implements OnInit {

  menus:any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
    {
      'col':[
      {'title': 'Registration', 'page': 'tenant-registration', 'img': 'sub-menu/administrasi/profile.png'},
      {'title': 'Change Password', 'page': 'change-password', 'img': 'sub-menu/administrasi/change_password.png'}
      ]
    }
    ];
  }

  back() {
    this.navCtrl.back();
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

}
