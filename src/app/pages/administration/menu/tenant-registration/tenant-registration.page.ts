import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
//import { SQLite } from '@ionic-native/sqlite/ngx';

import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';

@Component({
  selector: 'app-tenant-registration',
  templateUrl: './tenant-registration.page.html',
  styleUrls: ['./tenant-registration.page.scss'],
})
export class TenantRegistrationPage implements OnInit {

  registerForm: FormGroup;
  myPhoto: any;
  constructor(
    public navCtrl: NavController,
    public api: ApiService,
    public loading: LoadingService,
    public toastr: ToastrService,
    private storage: Storage,
    private formBuilder: FormBuilder,
    //private platform: Platform,
    //private sqlite: SQLite,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      handphone: ['', Validators.required],
      address: ['', Validators.required],
      id: ['']
      ///images: ['', Validators.required]
    });

    this.storage.get('currentUser').then((storage: any) => {
      this.registerForm.patchValue({
        id: storage.id,
        username: storage.username,
        name: storage.name,
        email: storage.email,
        phone: storage.phone,
        handphone: storage.phone,
        address: storage.address
      });
    });
  }

  async registerTenant() {
		const alert = await this.alertCtrl.create({
			subHeader: 'Registration Updated',
      message: 'Do you want to change this information?',
			buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Save',
          handler: () => {
            this.loading.present('Please wait...');
            this.api.updateData('update_tenant', this.registerForm.value).subscribe((data: any) => {
            
              this.toastr.present(data.message,'top');
            }, (error: any) => {

            }, () => {
              // this.storage.clear();
              // this.platform.ready().then(() => {
              //     this.sqlite.deleteDatabase({
              //       name: 'elife.db',
              //       location: 'default'
              //     });
              // });
              this.loading.dismiss();
              this.navCtrl.navigateRoot('home');
            });
          }
        }
      ]
		});
		await alert.present();
	}

  back() {
    this.navCtrl.back();
  }

  home() {
    this.navCtrl.navigateRoot('home');
  }
}
