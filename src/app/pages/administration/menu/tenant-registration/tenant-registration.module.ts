import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TenantRegistrationPageRoutingModule } from './tenant-registration-routing.module';
import { TenantRegistrationPage } from './tenant-registration.page';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TenantRegistrationPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [TenantRegistrationPage]
})
export class TenantRegistrationPageModule {}
