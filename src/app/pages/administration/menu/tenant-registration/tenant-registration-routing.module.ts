import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TenantRegistrationPage } from './tenant-registration.page';

const routes: Routes = [
  {
    path: '',
    component: TenantRegistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TenantRegistrationPageRoutingModule {}
