import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Platform, AlertController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { SQLite } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  resetPasswordForm : FormGroup;
  confirmed: boolean;
  constructor(
    public navCtrl: NavController,
    public api: ApiService,
    public loading: LoadingService,
    public toastr: ToastrService,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private platform: Platform,
    private sqlite: SQLite,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      prevPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {
      validator: this.checkConfirmation.bind(this)
    });
  }

  get f() { return this.resetPasswordForm.controls; }

  checkConfirmation(resetPasswordForm: FormGroup) {
    let newPassword = resetPasswordForm.controls.newPassword.value;
    let confirmPassword = resetPasswordForm.controls.confirmPassword.value;

    if (confirmPassword !== newPassword) {
      this.resetPasswordForm.controls['confirmPassword'].setErrors({'confirmed': true});
    }

    return null;

  }

  changePass(){
    this.loading.present('Please wait...');
    this.storage.get('currentUser').then((storage: any)=> {
      this.resetPasswordForm.value.id_tenant = storage.id;
      this.api.updateData('changepassword',this.resetPasswordForm.value).subscribe((data)=> {
        if (data != undefined) {
          this.toastr.present(data['message'],'middle');
          this.storage.clear();
          this.platform.ready().then(() => {
            this.sqlite.deleteDatabase({
              name: 'elife.db',
              location: 'default'
            });
          });
          this.navCtrl.navigateRoot('login');
          this.loading.dismiss();
        }
      });
    });
  }

  back() {
    this.navCtrl.back();
  }
}
