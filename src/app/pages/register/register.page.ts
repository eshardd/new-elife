import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';

import { ApiService } from '../../../services/api/api.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { ToastrService } from '../../../services/toastr/toastr.service';
import { Storage } from '@ionic/Storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  	registerForm : FormGroup;
  	constructor(
    	public api: ApiService,
 		public navCtrl: NavController,
 		//public navParams: NavParams,
 		//public toastCtrl: ToastController,
 		//public actionSheetCtrl: ActionSheetController,
 		public alertCtrl: AlertController,
 		public toastr: ToastrService,
 		public loading: LoadingService,
 		private storage: Storage,
 		private formBuilder: FormBuilder,
 		//private menuCtrl: MenuController,
		//private sqlite: SQLite,
 		//private platform: Platform
  	) { 	 
		this.registerForm = this.formBuilder.group({
			name: ['', Validators.required],
			email: ['', [Validators.required, Validators.email]],
			phone: ['', Validators.required],
			handphone: ['', Validators.required],
			address: ['', Validators.required],
			username: ['', Validators.required],
			//images: ['', Validators.required]
		});
  	}

  	ngOnInit() {
  	}

	async registerTenant() {
		const alert = await this.alertCtrl.create({
			subHeader: 'Confirmation Registration ?',
			message: 'Please check your information correctly',
			buttons: [{
				text: 'Back',
				handler: () => {
					//
				}
			  },
			  {
				text: 'Continue',
				handler: () => {
				  this.loading.present('Please wait...');
				  this.api.postData('new_tenant', this.registerForm.value).subscribe((data: any) => {
					const resultdata = data;
					this.toastr.present('Registered Success','middle');
				  }, (error: any) => {
					this.loading.dismiss();
					this.toastr.present(error.error.message,'middle');
				  }, () => {
					this.loading.dismiss();
					this.registerForm.reset();
					this.navCtrl.navigateRoot('login');
				  });
				}
			  }
			]
		  });
	
		await alert.present();
	}
	goTo(page){
		this.navCtrl.navigateRoot(page);
	}
	
}
