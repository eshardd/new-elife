import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessTenantsPageRoutingModule } from './business-tenants-routing.module';

import { BusinessTenantsPage } from './business-tenants.page';
import { NavParams } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessTenantsPageRoutingModule
  ],
  declarations: [BusinessTenantsPage],
  providers:[NavParams]
})
export class BusinessTenantsPageModule {}
