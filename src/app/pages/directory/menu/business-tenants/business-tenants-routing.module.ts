import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessTenantsPage } from './business-tenants.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessTenantsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessTenantsPageRoutingModule {}
