import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-business-tenants',
  templateUrl: './business-tenants.page.html',
  styleUrls: ['./business-tenants.page.scss'],
})
export class BusinessTenantsPage implements OnInit {

  public businessTenant: any;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
    this.getBusinessTenant();
  }

  getBusinessTenant() {
    this.loading.present("Please wait...");
    this.api.getData('business_tenant/all/').subscribe((data: any) => {
      this.businessTenant = data;
      this.loading.dismiss();
    });
	}

  back() {
    this.navCtrl.back();
  }

  home() {
    this.navCtrl.navigateRoot('home');
  }

}
