import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-residents',
  templateUrl: './residents.page.html',
  styleUrls: ['./residents.page.scss'],
})
export class ResidentsPage implements OnInit {

  public residents: any;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
    //console.log('ionViewDidLoad ResidentsPage');
    this.getResidents();
  }

  getResidents() {
    this.loading.present("Please wait...");
    this.api.getData('residents/all/').subscribe((data: any) => {
      this.residents = data;
      this.loading.dismiss();
    });
	}

  back() {
    this.navCtrl.back();
  }

  home() {
    this.navCtrl.navigateRoot('home');
  }
}
