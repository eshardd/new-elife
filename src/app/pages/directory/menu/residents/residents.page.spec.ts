import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResidentsPage } from './residents.page';

describe('ResidentsPage', () => {
  let component: ResidentsPage;
  let fixture: ComponentFixture<ResidentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResidentsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResidentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
