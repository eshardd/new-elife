import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicNumbersPageRoutingModule } from './public-numbers-routing.module';

import { PublicNumbersPage } from './public-numbers.page';
import { NavParams } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicNumbersPageRoutingModule
  ],
  declarations: [PublicNumbersPage],
  providers:[NavParams]
})
export class PublicNumbersPageModule {}
