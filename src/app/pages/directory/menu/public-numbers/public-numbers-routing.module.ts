import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicNumbersPage } from './public-numbers.page';

const routes: Routes = [
  {
    path: '',
    component: PublicNumbersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicNumbersPageRoutingModule {}
