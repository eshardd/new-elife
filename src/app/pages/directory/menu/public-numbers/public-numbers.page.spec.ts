import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicNumbersPage } from './public-numbers.page';

describe('PublicNumbersPage', () => {
  let component: PublicNumbersPage;
  let fixture: ComponentFixture<PublicNumbersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicNumbersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicNumbersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
