import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-public-numbers',
  templateUrl: './public-numbers.page.html',
  styleUrls: ['./public-numbers.page.scss'],
})
export class PublicNumbersPage implements OnInit {

  public publicNumber: any;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
    this.getPublicNumber()
  }

  getPublicNumber() {
    this.loading.present("Please wait...");
    this.api.getData('number/all/2/').subscribe((data: any) => {
      this.publicNumber = data;
      this.loading.dismiss();
    });
	}

  back() {
    this.navCtrl.back();
  }

  home(){
    this.navCtrl.navigateRoot('home');
  }
}
