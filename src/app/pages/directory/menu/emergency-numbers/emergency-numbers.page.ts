import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-emergency-numbers',
  templateUrl: './emergency-numbers.page.html',
  styleUrls: ['./emergency-numbers.page.scss'],
})
export class EmergencyNumbersPage implements OnInit {

  public emergencyNumber: any;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
    this.getEmergencyNumber()
  }

  getEmergencyNumber() {
    this.loading.present("Please wait...");
    this.api.getData('number/all/1/').subscribe((data: any) => {
      this.emergencyNumber = data;
      this.loading.dismiss();
    });
	}

  back() {
    this.navCtrl.back();
  }

  home(){
    this.navCtrl.navigateRoot('home');
  }

}
