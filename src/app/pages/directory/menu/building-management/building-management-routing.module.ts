import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildingManagementPage } from './building-management.page';

const routes: Routes = [
  {
    path: '',
    component: BuildingManagementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildingManagementPageRoutingModule {}
