import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, AlertController } from '@ionic/angular';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-building-management',
  templateUrl: './building-management.page.html',
  styleUrls: ['./building-management.page.scss'],
})
export class BuildingManagementPage implements OnInit {

  public buildingManagement: any;
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public alertCtrl: AlertController,
  ) { }

  ngOnInit() {
    //onsole.log('ionViewDidLoad BuildingManagementPage');
    this.getBuildingManagement()
  }

  getBuildingManagement() {
    this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any) => {
			this.api.getData('building_management/all/').subscribe((data: any) => {
        this.buildingManagement = data;
				this.loading.dismiss();
			});
		});
	}


  back() {
    this.navCtrl.back();
  }

  home() {
    this.navCtrl.navigateRoot('home');
  }

}
