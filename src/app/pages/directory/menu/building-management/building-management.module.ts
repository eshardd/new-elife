import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuildingManagementPageRoutingModule } from './building-management-routing.module';

import { BuildingManagementPage } from './building-management.page';
import { NavParams } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuildingManagementPageRoutingModule
  ],
  declarations: [BuildingManagementPage],
  providers:[NavParams]
})
export class BuildingManagementPageModule {}
