import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuildingManagementPage } from './building-management.page';

describe('BuildingManagementPage', () => {
  let component: BuildingManagementPage;
  let fixture: ComponentFixture<BuildingManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildingManagementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuildingManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
