import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.page.html',
  styleUrls: ['./directory.page.scss'],
})
export class DirectoryPage implements OnInit {

  menus:any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
    {
      'col':[
      {'title': 'Building Management', 'page': 'building-management', 'img': 'sub-menu/directory/building-management.png'},
      {'title': 'Business Tenants', 'page': 'business-tenants', 'img': 'sub-menu/directory/business-tenants.png'},
      {'title': 'Emergency Numbers', 'page': 'emergency-numbers', 'img': 'sub-menu/directory/emergency-numbers.png'},
      {'title': 'Public Numbers', 'page': 'public-numbers', 'img': 'sub-menu/directory/public-numbers.png'},
      {'title': 'Residents', 'page': 'residents', 'img': 'sub-menu/directory/residents.png'}
      ]
    }
    ];
  }

  goToPage(page: string) {
    console.log(page);
		this.navCtrl.navigateForward(page);
  }
  
  back() {
    this.navCtrl.back();
  }

}
