import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { ProcessOrderGoodsPage } from '../../../../components/process-order-goods/process-order-goods.page';
//import { FoodDetailPage } from './food-detail/food-detail.page';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.page.html',
  styleUrls: ['./goods.page.scss'],
})
export class GoodsPage implements OnInit {
  listStore:any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
    public loading: LoadingService,
    private modal			:ModalController,
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicePage');
  }

  ngOnInit(){
    this.getStore();
  }

  getStore() {
    this.loading.present('Please wait...');
     this.api.getData('order/master/ms_goods_store').subscribe((data: any)=> {
       this.listStore = data;
       this.loading.dismiss();
     });
   }

  goToHome() {
    this.navCtrl.navigateRoot('home');
  }

  //order(id: number,name:string) {
    //  const modal = this.modal.create(ProcessOrderGoodsComponent, { 'id': id,'name': name });
    //  modal.present();
   //}
  
  async order(id: number,name:string) {
		const modal = await this.modal.create({
		  component: ProcessOrderGoodsPage,
		  componentProps: {
        'id': id,
        'name':name
		  },
		});
		return await modal.present();
	}

  back() {
    this.navCtrl.back();
  }
}
