import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { ProcessOrderServicePage } from '../../../../components/process-order-service/process-order-service.page';

@Component({
  selector: 'app-service',
  templateUrl: './service.page.html',
  styleUrls: ['./service.page.scss'],
})
export class ServicePage implements OnInit {

  listStore:any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
    public loading: LoadingService,
    private modal			:ModalController,
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicePage');
  }

  ngOnInit(){
    this.getStore();
  }

  getStore() {
    this.loading.present("Please wait...");
    this.api.getData('order/master/ms_service_store').subscribe((data: any)=> {
      this.listStore = data;
      this.loading.dismiss();
    });
   }

  async order(id: number,name:string) {
     const modal = await this.modal.create({
		  component: ProcessOrderServicePage,
		  componentProps: {
        'id': id,
        'name':name
		  },
		});
		return await modal.present();
   }

  back() {
    this.navCtrl.back();
  }

  home() {
    this.navCtrl.navigateRoot('home');
  }
}
