import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../../../../services/api/api.service';
import { LoadingService } from '../../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { ProcessDisputePage } from '../../../../../components/process-dispute/process-dispute.page';
import { InvoiceViewPage } from '../../../../../components/invoice-view/invoice-view.page';

@Component({
  selector: 'app-food-detail',
  templateUrl: './food-detail.page.html',
  styleUrls: ['./food-detail.page.scss'],
})
export class FoodDetailPage implements OnInit {

  name: string;
 	id: number;
 	images: string;
 	description: string;
 	orderForm : FormGroup;
 	listMenu: any;
 	list_order: any;
 	total: number;
  foodNavDetail: string = 'menu';
   
  constructor(
    public navCtrl	: NavController, 
		public navParams		: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
  ) {
    this.id = navParams.data.data.id;
 		this.name = navParams.data.data.name;
 		this.images = navParams.data.data.images;
 		this.description = navParams.data.data.description;

     console.log(navParams.data.data);
 		if(this.id === undefined) {
				this.navCtrl.navigateRoot('food');
 		} else{
 			this.getMenu(this.id);
 		}
  }

  get f() {
    return this.orderForm.controls;
  }
  get myMenu() {
    return this.orderForm.controls['myMenu'] as FormArray;
  }

  ngOnInit() {
    this.list_order = [];
    this.listMenu = [];
    this.orderForm = this.formBuilder.group({
      tenant_id: [''],
      tenant_address: [''],
      store: [this.id, Validators.required],
      myMenu: this.formBuilder.array([])
    });
  }

  getMenu(id: number) {
    this.loading.present("Please wait...");
    this.api.getData('order/master/ms_food_menu/'+id).subscribe((data: any)=> {
      this.listMenu = data;
      data.forEach((v, k)=>{
        this.myMenu.push(this.formBuilder.group(
        {
          menu: ['', Validators.required],
          qty: [1, Validators.required]
        }
        ));
      });
      this.loading.dismiss();
    });
  }

 plus(id_food: number, index: number){

    var selected_food = this.listMenu.find(x => x.id == id_food);

    if(this.list_order.find(x => x.id == selected_food.id)) {
      selected_food.qty = selected_food.qty+1;
      selected_food.subtotal = selected_food.qty * selected_food.amount;
   }else{
     selected_food.qty = 1;
     selected_food.subtotal = selected_food.qty * selected_food.amount;
     this.list_order.push(selected_food);
   }

   this.myMenu.controls[index].patchValue({
     menu: id_food,
     qty: selected_food.qty
   });

   this.count_total();
  }


  min(id_food: number, index: number){
    const selected_food = this.listMenu.find(x => x.id == id_food);

    if (selected_food.qty>0) {
      selected_food.qty = selected_food.qty-1;
      selected_food.subtotal = selected_food.qty * selected_food.amount;
      if(selected_food.qty == 0){
        this.list_order.pop(x => x.id == selected_food.id);
      }
    }else if(selected_food.qty == 0){
      this.list_order.pop(x => x.id == selected_food.id);
    }
    this.myMenu.controls[index].patchValue({
      menu: id_food,
      qty: selected_food.qty
    });
    this.count_total();
  }

  count_total(){
    var count = 0;
    this.list_order.forEach(function(value, key) {
      count = count + value.subtotal;
    });
    this.total = count;
  }

  purchase(){
    this.loading.present("Please wait...");

   this.storage.get('currentUser').then((storage: any)=> {
     this.orderForm.get('tenant_id').setValue(storage.id);
     this.orderForm.get('tenant_address').setValue(storage.address);
     this.api.postData('order/food_order', this.orderForm.value).subscribe((data: any)=> {
       this.toastr.present(data.message,'top');
       this.orderForm.reset();
       this.loading.dismiss();
     });
   });

    setTimeout(()=>{
      this.modal.dismiss();
    }, 2000);

    /*this.api.postData('order/master/ms_food_menu/'+id).subscribe((data: any)=> {

    });*/
    /*setTimeout(()=>{
      loader.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Thankyou',
        subTitle: 'thankyou for your purchase',
        buttons: ['Dismiss']
      });
      alert.present();
    }, 3000);

    setTimeout(()=>{
      this.navCtrl.push('FoodPage');
    }, 4000);*/
  }


  back(){
		this.modal.dismiss();
  }
  
  home(){
    this.modal.dismiss();
    this.navCtrl.navigateRoot('home');
	}
}
