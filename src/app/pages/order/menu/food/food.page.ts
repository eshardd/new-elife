import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController ,LoadingController, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { ApiService } from '../../../../../services/api/api.service';
import { LoadingService } from '../../../../../services/loading/loading.service';
import { ToastrService } from '../../../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';
import { FoodDetailPage } from './food-detail/food-detail.page';

@Component({
  selector: 'app-food',
  templateUrl: './food.page.html',
  styleUrls: ['./food.page.scss'],
})
export class FoodPage implements OnInit {

  listStore:any;
 	listMenu: any;
 	storeId: number;
 	orderForm : FormGroup;
 	myFoodOrder: any;
 	segmenOrder: string = 'order';
 	list_order: any;
  constructor(
    public navCtrl: NavController,
		public navParams: NavParams,
		public api: ApiService,
		private formBuilder: FormBuilder,
		private storage: Storage,
		public toastr: ToastrService,
    public loading: LoadingService,
    private modal			:ModalController,
  ) {
    this.getStore();
 		//this.getMenu();
 		//this.getListOrder();
 		this.orderForm = this.formBuilder.group({
 			store: ['', Validators.required],
 			myMenu: this.formBuilder.array([this.formBuilder.group({
 				menu: ['', Validators.required],
 				qty: [1, Validators.required]
 			})])
 		});
  }

  get f() { return this.orderForm.controls; }

 	get myMenu() {
 		return this.orderForm.get('myMenu') as FormArray;
 	}


  ngOnInit() {
  }

  getListOrder() {
    this.api.getData('order/list_food_order/1').subscribe((data: any)=> {
      if(data.status !== '20004'){
        this.list_order = data.data;
      } else {
        this.list_order = {};
      }
    });
  }

  getStore() {
    this.loading.present("Please wait...");
    this.api.getData('order/master/ms_food_store').subscribe((data: any)=> {
      this.listStore = data;
      this.loading.dismiss();
    });
  }

  getMenu() {
    this.api.getData('order/master/ms_food_menu').subscribe((data: any)=> {
      this.listMenu = data;
    });
  }

  addMore() {
    this.myMenu.push(this.formBuilder.group(
    {
      menu: ['', Validators.required],
      qty: [1, Validators.required]
    }
    ));

  }

  removeMenu(index) {
    if(this.myMenu.length > 1){
      this.myMenu.removeAt(index);
    } else {
      return false;
    }
  }

  pushOrder() {
    this.api.postData('order/food_order', this.orderForm.value).subscribe((data: any)=> {
      this.toastr.present(data.message,'top');
      this.orderForm.reset();
    });
  }

  changeSegment(event) {
    if(event.value === 'list_order') {
      this.getListOrder();
    }
  }

  // goToPage(page: string, data: any) {
  //   this.navCtrl.navigateForward(page,);
  // }

  async goToPage(page: string, data: any) {
		const modal = await this.modal.create({
		  component: FoodDetailPage,
		  componentProps: {
        'page': page,
        'data': data
		  },
		});
		return await modal.present();
	}

  back() {
    this.navCtrl.back();
  }
  home() {
    this.navCtrl.navigateRoot('home');
  }
}
