import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {

  menus: any;
  constructor(
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.menus = [
      {
        'col':[
        {'title': 'Food', 'page': 'food', 'img': 'sub-menu/order/food.png'},
        {'title': 'Goods', 'page': 'goods', 'img': 'sub-menu/order/goods.png'},
        {'title': 'Service', 'page': 'service', 'img': 'sub-menu/order/service.png'},
        ]
      }
      ];
  }

  goToPage(page) {
    this.navCtrl.navigateForward(page);
  }

  back() {
    this.navCtrl.back();
  }

}
