import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';  
import { RouteReuseStrategy } from '@angular/router';


import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { StreamingMedia } from '@ionic-native/streaming-media/ngx';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//service
import { ApiService } from '../services/api/api.service';
import { GlobalService } from '../services/global/global.service';

// httpclient
import { HttpClientModule } from '@angular/common/http';

// angular form
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// storage
import { IonicStorageModule, Storage } from '@ionic/storage';

// jwt
import { JwtModule, JWT_OPTIONS } from "@auth0/angular-jwt";

// sqlite
import { SQLite } from '@ionic-native/sqlite/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
//import { DocumentViewer } from '@ionic-native/document-viewer/ngx';

export function jwtOptionsFactory(storage) {
  return {
    tokenGetter: () => {
      return storage.get('token');
    },
    whitelistedDomains: ['localhost:8100']
  }
}

var config = {
  backButtonText: '',
  backButtonIcon: 'md-arrow-back',
  tabsPlacement: 'bottom',
  pageTransition: 'ios',
  menuType: 'overlay'
};

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(config),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Storage]
      }
    }),
  ],
  providers: [
    File,
    //DocumentViewer,
    FileTransfer,
    FileOpener,
    Camera,
    Base64,
    ApiService,
    GlobalService,
    StatusBar,
    SplashScreen,
    SQLite,
    StreamingMedia,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
