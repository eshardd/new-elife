import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../services/api/api.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { ToastrService } from '../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-invoice-view',
  templateUrl: './invoice-view.page.html',
  styleUrls: ['./invoice-view.page.scss'],
})
export class InvoiceViewPage implements OnInit {

  invoice_id:number;
 	showcontent:boolean = false;

 	name: any;
 	address: any;
 	invoice_no: any;
 	listitem: any;
 	tax:any;
  total: any;
   
  constructor(
    public navCtrl	: NavController, 
		public params: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
  ) { }

  ngOnInit() {
    this.invoice_id 		= this.params.data.invoice;

 		this.api.getData('invoicedata/'+this.invoice_id).subscribe((data: any)=> {
 			this.invoice_no		= data.invoice_no;
	 		this.name			= data.tenant_name;
	 		this.address		= data.invoice_address;
 		});

 		this.invoicedetail(this.invoice_id);
  }

  invoicedetail(invoice_id:number){
    this.loading.present("Please wait...");

    this.api.getData('invoicedetail/'+invoice_id).subscribe((data: any)=> {
      this.listitem			= data;
      if (this.listitem) {
        this.count_total(this.listitem);
        this.showcontent = true;
        this.loading.dismiss();
      }
      
    });
  }

  count_total(item){
    var count = 0;
    item.forEach(function(value, key) {
      count = count + parseInt(value.price);
    });
    this.tax = count/10;
    this.total = count + this.tax;
  }

  close() {
    this.modal.dismiss();
  }

}
