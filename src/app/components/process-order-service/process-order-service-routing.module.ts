import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProcessOrderServicePage } from './process-order-service.page';

const routes: Routes = [
  {
    path: '',
    component: ProcessOrderServicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProcessOrderServicePageRoutingModule {}
