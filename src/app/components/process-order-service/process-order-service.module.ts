import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProcessOrderServicePageRoutingModule } from './process-order-service-routing.module';

import { ProcessOrderServicePage } from './process-order-service.page';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ProcessOrderServicePageRoutingModule
  ],
  declarations: [ProcessOrderServicePage]
})
export class ProcessOrderServicePageModule {}
