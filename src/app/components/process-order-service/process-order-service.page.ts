import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../services/api/api.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { ToastrService } from '../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-process-order-service',
  templateUrl: './process-order-service.page.html',
  styleUrls: ['./process-order-service.page.scss'],
})
export class ProcessOrderServicePage implements OnInit {

  text: string;
 	myParam: number;
 	serviceForm : FormGroup;
 	goods_id: any;
 	goods_name: any;
  dispute_type:any;
   
  constructor(
    public params: NavParams, 
 		private formBuilder: FormBuilder,
 		public api: ApiService,
 		public toastr: ToastrService,
 		public loading: LoadingService,
 		public navCtrl: NavController,
 		private storage: Storage,
    private modal			:ModalController, 
    ) {
 		this.serviceForm = this.formBuilder.group({
			tenant_id		: 	'',
			service_store_id		: 	[''],
			date_start		: 	['', Validators.required],
			notes			: 	['', Validators.required],
    });
  }

  get f() { return this.serviceForm.controls; }

 	ngOnInit(){
 		this.goods_id 		= this.params.data.id;
 		this.goods_name 	= this.params.data.name;
 		this.getdropdown();
 	}

 	dismiss() {
 		this.modal.dismiss();
 	}

 	getdropdown(){
    this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any)=> {
			this.api.getData('billing/dispute/dropdown/'+storage.id).subscribe((data: any)=> {
				this.dispute_type = data.dispute_type;
				this.loading.dismiss();
			});
		});
	}

 	disputeProcess(){
     this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any)=> {
			this.serviceForm.get('tenant_id').setValue(storage.id);
			this.serviceForm.get('service_store_id').setValue(this.goods_id);
			this.api.postData('order/service_order',this.serviceForm.value).subscribe((data)=> {
				if (data != undefined) {
          this.toastr.present("Success","top");
					this.loading.dismiss();
 					this.modal.dismiss();
				}
			});
		});
 	}

  close() {
    this.modal.dismiss();
}

home() {
	this.modal.dismiss();
    this.navCtrl.navigateRoot('home');
  }

}
