import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProcessOrderServicePage } from './process-order-service.page';

describe('ProcessOrderServicePage', () => {
  let component: ProcessOrderServicePage;
  let fixture: ComponentFixture<ProcessOrderServicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessOrderServicePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProcessOrderServicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
