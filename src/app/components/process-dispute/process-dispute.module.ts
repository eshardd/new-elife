import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProcessDisputePageRoutingModule } from './process-dispute-routing.module';

import { ProcessDisputePage } from './process-dispute.page';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProcessDisputePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ProcessDisputePage]
})
export class ProcessDisputePageModule {}
