import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../services/api/api.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { ToastrService } from '../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-process-dispute',
  templateUrl: './process-dispute.page.html',
  styleUrls: ['./process-dispute.page.scss'],
})
export class ProcessDisputePage implements OnInit {

  text: string;
 	myParam: number;
 	disputeForm : FormGroup;
 	invoice_id: any;
  dispute_type:any;

  constructor(
    public navCtrl	: NavController, 
		public params: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
  ) {
    this.disputeForm = this.formBuilder.group({
			tenant_id		: 	'',
			invoice_id		: 	[''],
			type			: 	['', Validators.required],
			description		: 	['', Validators.required],
		});
  }

  ngOnInit() {
    this.invoice_id 		= this.params.data.id;
 		this.getdropdown();
  }

  getdropdown(){
		this.loading.present('Please wait...');
		this.storage.get('currentUser').then((storage: any)=> {
			this.api.getData('billing/dispute/dropdown/'+storage.id).subscribe((data: any)=> {
				this.dispute_type = data.dispute_type;
        this.loading.dismiss();
        console.log(this.dispute_type)
			});
		});
  }
  
  disputeProcess(){
    this.loading.present('Please wait...');
    this.storage.get('currentUser').then((storage: any)=> {
    this.disputeForm.patchValue({tenant_id: storage.id});
    this.disputeForm.patchValue({invoice_id: this.invoice_id});
    this.api.postData('billing/dispute/add',this.disputeForm.value).subscribe((data)=> {
       if (data != undefined) {
         this.toastr.present('Success','top');
         this.loading.dismiss();
         this.close();
       }
     });
   });
  }

  close() {
    this.modal.dismiss();
  }

}
