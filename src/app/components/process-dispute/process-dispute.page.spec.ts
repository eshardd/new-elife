import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProcessDisputePage } from './process-dispute.page';

describe('ProcessDisputePage', () => {
  let component: ProcessDisputePage;
  let fixture: ComponentFixture<ProcessDisputePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessDisputePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProcessDisputePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
