import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProcessDisputePage } from './process-dispute.page';

const routes: Routes = [
  {
    path: '',
    component: ProcessDisputePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProcessDisputePageRoutingModule {}
