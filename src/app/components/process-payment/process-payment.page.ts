import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams, ActionSheetController, AlertController } from '@ionic/angular';
import { ApiService } from '../../../services/api/api.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { ToastrService } from '../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-process-payment',
  templateUrl: './process-payment.page.html',
  styleUrls: ['./process-payment.page.scss'],
})
export class ProcessPaymentPage implements OnInit {

  text: string;
 	myParam: number;
 	paymentForm : FormGroup;
 	invoice_id: any;
 	myPhoto:any;
 	raw_image:string;

  constructor(
    public navCtrl	: NavController, 
		public params: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
    private modal			:ModalController,
    public actionSheetCtrl: ActionSheetController,
    private base64: Base64,
		private camera: Camera,
    private sanitize: DomSanitizer,
    public alertCtrl: AlertController,
  ) { 
    this.paymentForm = this.formBuilder.group({
      tenant_id: '',
      invoice_id: [''],
      bank_account_name: ['', Validators.required],
      bank_account_number: ['', Validators.required],
      amount: ['', Validators.required],
      payment_date: ['', Validators.required],
      images: [null]
    });
  }

  get f() { return this.paymentForm.controls; }

  ngOnInit(){
    this.invoice_id 		= this.params.data.id;
  }

  async takePhoto() {

   const actionSheet = await this.actionSheetCtrl.create({
    header: 'Take the picture',
     buttons: [
     {
       text: 'Take a picture',
       handler: () => {
         this.fromCamera();
       }
     },
     {
       text: 'Take from gallery',
       handler: () => {
         this.formGallery();
       }
     }
     ]
   });
   await actionSheet.present();
 }


 fromCamera() {

   const options: CameraOptions = {
     quality: 80,
     destinationType: this.camera.DestinationType.FILE_URI,
     encodingType: this.camera.EncodingType.JPEG,
     mediaType: this.camera.MediaType.PICTURE
   }

   this.camera.getPicture(options).then((imageData) => {
    // imageData is either a base64 encoded string or a file URI
    // If it's base64 (DATA_URL):

    let filePath: string = imageData;
    this.base64.encodeFile(filePath).then((base64File: string) => {
     this.myPhoto = this.sanitize.bypassSecurityTrustUrl(base64File);
     this.raw_image = base64File;
    }, (err) => {
      console.log(err);
    });


   }, (err) => {
    //  const alert = this.alertCtrl.create({
    //    title: 'Take image from camera failed',
    //    subTitle: err,
    //    buttons: ['OK']
    //  });
    //  alert.present();
   });
 }

 formGallery() {
   const options: CameraOptions = {
     quality: 80,
     destinationType: this.camera.DestinationType.FILE_URI,
     encodingType: this.camera.EncodingType.JPEG,
     mediaType: this.camera.MediaType.PICTURE,
     sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
   }

   this.camera.getPicture(options).then((imageData) => {

     let url_temp: string;
     if (imageData.substring(0,21)== "content://com.android") {
       let photo_split: any= imageData.split("%3A");
       url_temp="content://media/external/images/media/"+photo_split[1];
     }

     this.base64.encodeFile(url_temp).then((base64File: string) => {
       this.myPhoto = this.sanitize.bypassSecurityTrustUrl(base64File);
       //this.myPhoto = this.sanitize.bypassSecurityTrustResourceUrl(base64File);
       this.raw_image = base64File;
     }, (err) => {
       console.log(err);
     });

     }, (err) => {
    // Handle error
    // const alert = this.alertCtrl.create({
    //  title: 'Take image from gallery failed',
    //  subTitle: err,
    //  buttons: ['OK']
    // });
    // alert.present();
    alert("error");
   });
 }


  paymentProcess(){


    //this.loading.present("Please wait...");
    this.storage.get('currentUser').then((storage: any)=> {
    
    this.paymentForm.patchValue({tenant_id: storage.id});
    this.paymentForm.patchValue({invoice_id: this.invoice_id});
    this.paymentForm.patchValue({images: this.raw_image});
    
    this.api.postData('billing/payment/add',this.paymentForm.value).subscribe((data)=> {
      //alert(JSON.stringify(data));
         this.toastr.present("Success","top");
         //this.loading.dismiss();
         this.modal.dismiss();
     });
   });
  }

  close() {
    this.modal.dismiss();
  }
}
