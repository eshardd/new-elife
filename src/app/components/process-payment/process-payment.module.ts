import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProcessPaymentPageRoutingModule } from './process-payment-routing.module';

import { ProcessPaymentPage } from './process-payment.page';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ProcessPaymentPageRoutingModule
  ],
  declarations: [ProcessPaymentPage]
})
export class ProcessPaymentPageModule {}
