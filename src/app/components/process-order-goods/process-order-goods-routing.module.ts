import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProcessOrderGoodsPage } from './process-order-goods.page';

const routes: Routes = [
  {
    path: '',
    component: ProcessOrderGoodsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProcessOrderGoodsPageRoutingModule {}
