import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProcessOrderGoodsPageRoutingModule } from './process-order-goods-routing.module';

import { ProcessOrderGoodsPage } from './process-order-goods.page';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ProcessOrderGoodsPageRoutingModule
  ],
  declarations: [ProcessOrderGoodsPage]
})
export class ProcessOrderGoodsPageModule {}
