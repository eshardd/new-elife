import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../services/api/api.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { ToastrService } from '../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-process-order-goods',
  templateUrl: './process-order-goods.page.html',
  styleUrls: ['./process-order-goods.page.scss'],
})
export class ProcessOrderGoodsPage implements OnInit {

  text: string;
 	myParam: number;
 	goodsForm : FormGroup;
 	goods_id: any;
 	goods_name: any;
  dispute_type:any;
   
  constructor(
    public navCtrl	: NavController, 
		public params: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
  ) {
    this.goodsForm = this.formBuilder.group({
			tenant_id		: 	'',
			goods_id		: 	[''],
			date_start		: 	['', Validators.required],
			date_end		: 	['', Validators.required],
			notes			: 	['', Validators.required],
		});
  }

  get f() { return this.goodsForm.controls; }

 	ngOnInit(){
 		this.goods_id 		= this.params.data.id;
 		this.goods_name 	= this.params.data.name;
 		this.getdropdown();
 	}

 	dismiss() {
 		this.modal.dismiss();
 	}

 	getdropdown(){
     this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any)=> {
			this.api.getData('billing/dispute/dropdown/'+storage.id).subscribe((data: any)=> {
				this.dispute_type = data.dispute_type;
				this.loading.dismiss();
			});
		});
	}

 	disputeProcess(){
     this.loading.present("Please wait...");
		this.storage.get('currentUser').then((storage: any)=> {
			this.goodsForm.get('tenant_id').setValue(storage.id);
			this.goodsForm.get('goods_id').setValue(this.goods_id);
			this.api.postData('order/goods_order',this.goodsForm.value).subscribe((data)=> {
				if (data != undefined) {
					this.toastr.present("Success","top");
					this.loading.dismiss();
					this.navCtrl.pop();
				}
			});
		});
 	}

  close() {
    this.modal.dismiss();
  }

}
