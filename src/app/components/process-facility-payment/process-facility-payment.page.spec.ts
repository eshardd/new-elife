import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProcessFacilityPaymentPage } from './process-facility-payment.page';

describe('ProcessFacilityPaymentPage', () => {
  let component: ProcessFacilityPaymentPage;
  let fixture: ComponentFixture<ProcessFacilityPaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessFacilityPaymentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProcessFacilityPaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
