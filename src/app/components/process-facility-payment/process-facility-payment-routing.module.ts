import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProcessFacilityPaymentPage } from './process-facility-payment.page';

const routes: Routes = [
  {
    path: '',
    component: ProcessFacilityPaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProcessFacilityPaymentPageRoutingModule {}
