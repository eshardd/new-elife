import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProcessFacilityPaymentPageRoutingModule } from './process-facility-payment-routing.module';

import { ProcessFacilityPaymentPage } from './process-facility-payment.page';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ProcessFacilityPaymentPageRoutingModule
  ],
  declarations: [ProcessFacilityPaymentPage]
})
export class ProcessFacilityPaymentPageModule {}
