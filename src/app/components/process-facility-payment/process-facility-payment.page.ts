import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, NavController, ToastController, NavParams } from '@ionic/angular';
import { ApiService } from '../../../services/api/api.service';
import { LoadingService } from '../../../services/loading/loading.service';
import { ToastrService } from '../../../services/toastr/toastr.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-process-facility-payment',
  templateUrl: './process-facility-payment.page.html',
  styleUrls: ['./process-facility-payment.page.scss'],
})
export class ProcessFacilityPaymentPage implements OnInit {

  text: string;
 	myParam: number;
 	facilityPayment : FormGroup;
 	payment_id: any;
 	myPhoto:any;
  raw_image:string;
   
  constructor(
    public navCtrl	: NavController, 
		public params: NavParams,
		public api				: ApiService,
		private formBuilder		: FormBuilder,
		private storage			: Storage,
		public toastr		: ToastrService,
		public loading		: LoadingService,
		private modal			:ModalController,
  ) {
    this.facilityPayment = this.formBuilder.group({
      id: '',
      bank_account_name: ['', Validators.required],
      bank_account_number: ['', Validators.required],
      amount: ['', Validators.required],
      payment_date: ['', Validators.required],
      images: ['']
    });
  }

  get f() { return this.facilityPayment.controls; }

 	ngOnInit(){
 		this.payment_id 		= this.params.data.id;
 	}

 	// takePhoto() {

	// 	const actionSheet = this.actionSheetCtrl.create({
	// 		title: 'Take the picture',
	// 		buttons: [
	// 		{
	// 			text: 'Take a picture',
	// 			handler: () => {
	// 				this.fromCamera();
	// 			}
	// 		},
	// 		{
	// 			text: 'Take from gallery',
	// 			handler: () => {
	// 				this.formGallery();
	// 			}
	// 		}
	// 		]
	// 	});
	// 	actionSheet.present();
	// }


	// fromCamera() {

	// 	const options: CameraOptions = {
	// 		quality: 80,
	// 		destinationType: this.camera.DestinationType.FILE_URI,
	// 		encodingType: this.camera.EncodingType.JPEG,
	// 		mediaType: this.camera.MediaType.PICTURE
	// 	}

	// 	this.camera.getPicture(options).then((imageData) => {
	// 	 // imageData is either a base64 encoded string or a file URI
	// 	 // If it's base64 (DATA_URL):

	// 	 let filePath: string = imageData;
	// 	 this.base64.encodeFile(filePath).then((base64File: string) => {
	// 		this.myPhoto = this.sanitize.bypassSecurityTrustUrl(base64File);
	// 		this.raw_image = base64File;
	// 	 }, (err) => {
	// 		 console.log(err);
	// 	 });


	// 	}, (err) => {
	// 		const alert = this.alertCtrl.create({
	// 			title: 'Take image from camera failed',
	// 			subTitle: err,
	// 			buttons: ['OK']
	// 		});
	// 		alert.present();
	// 	});
	// }

	// formGallery() {
	// 	const options: CameraOptions = {
	// 		quality: 80,
	// 		destinationType: this.camera.DestinationType.FILE_URI,
	// 		encodingType: this.camera.EncodingType.JPEG,
	// 		mediaType: this.camera.MediaType.PICTURE,
	// 		sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
	// 	}

	// 	this.camera.getPicture(options).then((imageData) => {

	// 		let url_temp: string;
	// 		if (imageData.substring(0,21)== "content://com.android") {
	// 			let photo_split: any= imageData.split("%3A");
	// 			url_temp="content://media/external/images/media/"+photo_split[1];
	// 		}

	// 		this.base64.encodeFile(url_temp).then((base64File: string) => {
	// 			this.myPhoto = this.sanitize.bypassSecurityTrustUrl(base64File);
	// 			//this.myPhoto = this.sanitize.bypassSecurityTrustResourceUrl(base64File);
	// 			this.raw_image = base64File;
	// 		}, (err) => {
	// 			console.log(err);
	// 		});

	// 		}, (err) => {
	// 	 // Handle error
	// 	 const alert = this.alertCtrl.create({
	// 		title: 'Take image from gallery failed',
	// 		subTitle: err,
	// 		buttons: ['OK']
	// 	 });
	// 	 alert.present();
	// 	});
	// }

 	paymentProcess(){
     this.loading.present("Please wait...");
 		this.facilityPayment.get('id').setValue(this.payment_id);
 		this.facilityPayment.get('images').setValue(this.raw_image);
 		this.api.updateData('facility/confirmation',this.facilityPayment.value).subscribe((data: any)=> {
 			if (data.id != '') {
         this.toastr.present("Success","top");
				this.loading.dismiss();
 			}
 		});
 		setTimeout(()=>{
 			this.modal.dismiss();
   		}, 1500);
 	}

  close() {
    this.modal.dismiss();
  }

}
