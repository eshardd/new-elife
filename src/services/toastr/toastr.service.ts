import { Injectable } from '@angular/core';
import { ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastrService {

  constructor(
    public toastCtrl: ToastController,
  ) { }

  async present(msg,position) {
		const toast = await this.toastCtrl.create({
			message: msg,
			duration: 2000,
			position: position,
		});
		toast.present();
	}
}