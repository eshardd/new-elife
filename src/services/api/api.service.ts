import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalService } from '../global/global.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url: string = 'http://localhost/api-scheduler/public/';
  private header: any;

  constructor(
    private http: HttpClient, 
    private global: GlobalService
   ) { }

  public getData(table: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      //'Authorization': 'Bearer '+token
    });
    return this.http.get(this.global._urlApi+table, {headers});
  }

  public updateData(table: string, body: object) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      //'Authorization': 'Bearer '+token
    });
    return this.http.put(this.global._urlApi+table, body, {headers});
  }

  public postData(table: string, body: object) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      //'Authorization': 'Bearer '+token
    });
    return this.http.post(this.global._urlApi+table, body, {headers})
  }

  public checkRestrict(token: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    });
    return this.http.get(this.global._urlApi+'restricted', {headers});
  }
}
